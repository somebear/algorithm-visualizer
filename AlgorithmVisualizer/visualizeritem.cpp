/*

    Copyright (c) 2012, Jonas Rabbe
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this
       list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
    ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#include "visualizeritem.h"
#include <qapplication.h>
#include <qdir.h>
#include <qpainter.h>
#include <qpluginloader.h>
#include <qdebug.h>

VisualizerItem::VisualizerItem(QDeclarativeItem *parent)
    : QDeclarativeItem(parent)
    , m_selectedAlgorithm(0)
    , m_runningTime(0)
{
    setFlag(QGraphicsItem::ItemHasNoContents, false);
    m_data = new VisualizerData();
    m_data->resize(width());

    loadPlugins();

    if (m_algorithms.count() > 0) {
        m_selectedAlgorithm = m_algorithms.values().first();
        emit algorithmsChanged();
    }
}

void VisualizerItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *options, QWidget *widget)
{
    Q_UNUSED(options);
    Q_UNUSED(widget);

    painter->fillRect(boundingRect(), Qt::black);

    painter->setPen(Qt::red);
    for (int i = 0; i < m_data->length(); i++) {
        painter->drawLine(i, height(), i, height() - ((qreal)(m_data->at(i)) / MAX_DATA_VALUE) * height());
    }
}

void VisualizerItem::start()
{
    m_runningTime = 0;
    if (m_selectedAlgorithm)
        m_selectedAlgorithm->start();
}

void VisualizerItem::stop()
{
    if (m_selectedAlgorithm)
        m_selectedAlgorithm->stop();
}

void VisualizerItem::reset()
{
    stop();
    m_data->reset();
    update();
}

bool VisualizerItem::isActive() const
{
    if (m_selectedAlgorithm)
        return m_selectedAlgorithm->isRunning();

    return false;
}

QStringList VisualizerItem::algorithms()
{
    return m_algorithms.keys();
}

QString VisualizerItem::selectedAlgorithm() const
{
    if (m_selectedAlgorithm)
        return m_selectedAlgorithm->name();

    return QString();
}

void VisualizerItem::setSelectedAlgorithm(QString key)
{
    if (!m_algorithms.contains(key))
        return;

    reset();
    m_selectedAlgorithm = m_algorithms.value(key);

    emit selectedAlgorithmChanged();
}

int VisualizerItem::runningTime() const
{
    return m_runningTime;
}

void VisualizerItem::geometryChanged(const QRectF &newGeometry, const QRectF &oldGeometry)
{
    if (oldGeometry.width() != newGeometry.width())
        emit widthChanged();

    if (oldGeometry.height() != newGeometry.height())
        emit heightChanged();

    stop();
    m_data->resize(newGeometry.width());
    update();
}

void VisualizerItem::doUpdate()
{
    update();
}

void VisualizerItem::handleRunningTime(int msec)
{
    m_runningTime = msec;

    emit algorithmCompleted();
}

void VisualizerItem::loadPlugins()
{
    foreach (QObject *plugin, QPluginLoader::staticInstances())
        handlePlugin(plugin);

    QDir pluginsDir = QDir(qApp->applicationDirPath());

#if defined(Q_OS_WIN)
    if (pluginsDir.dirName().toLower() == "debug" || pluginsDir.dirName().toLower() == "release")
        pluginsDir.cdUp();
#elif defined(Q_OS_MAC)
    if (pluginsDir.dirName() == "MacOS") {
        pluginsDir.cdUp();
    }
#endif
    pluginsDir.cd("PlugIns");

    foreach (QString fileName, pluginsDir.entryList(QDir::Files)) {
        QPluginLoader loader(pluginsDir.absoluteFilePath(fileName));
        QObject *plugin = loader.instance();
        if (plugin) {
            if (!handlePlugin(plugin))
                loader.unload();
        }
    }

}

bool VisualizerItem::handlePlugin(QObject *object)
{
    AbstractSortingPlugin *plugin = qobject_cast<AbstractSortingPlugin *>(object);
    if (plugin) {
        plugin->setData(m_data);
        connect(plugin, SIGNAL(update()), SLOT(doUpdate()));
        connect(plugin, SIGNAL(started()), SIGNAL(activeChanged()));
        connect(plugin, SIGNAL(finished()), SIGNAL(activeChanged()));
        connect(plugin, SIGNAL(runningTime(int)), SLOT(handleRunningTime(int)));
        m_algorithms.insert(plugin->name(), plugin);
        return true;
    }

    return false;
}

