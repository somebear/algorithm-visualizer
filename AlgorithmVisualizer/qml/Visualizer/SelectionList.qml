/*

    Copyright (c) 2012, Jonas Rabbe
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this
       list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
    ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

import QtQuick 1.0

Rectangle {
    id: root
    width: 150
    height: 30

    property alias text: label.text
    property alias model: list.model

    signal selected(string item)

    color: "transparent"

    Rectangle {
        id: backdrop
        anchors.fill: parent
        color: "transparent"

        MouseArea {
            anchors.fill: parent;
            onClicked: root.state = "";
        }
    }

    Rectangle {
        id: button

        anchors.fill: parent;

        gradient: Gradient {
            GradientStop { id: stop1; position: 0.0; color: "white" }
            GradientStop { id: stop2; position: 1.0; color: "grey" }
        }
        radius: 5
        border.color: "black"
        border.width: 1
        smooth: true

        opacity: 1.0
        visible: opacity != 0.0

        Text {
            id: label
            anchors { verticalCenter: parent.verticalCenter; left: parent.left; leftMargin: 10 }
            font.family: "Helvetica Neue"
        }

        MouseArea {
            anchors.fill: parent;
            onClicked: root.state = "EXPANDED";
        }

    }

    Rectangle {
        id: listContainer
        anchors { left: parent.left; bottom: parent.bottom }
        width: parent.width / 3
        height: parent.height / 2 + 15
        anchors.margins: 10
        opacity: 0.0
        visible: opacity != 0.0
        color: "white"
        clip: true

        border { width: 1; color: "black" }
        smooth: true
        radius: 5

        ListView {
            id: list
            anchors.fill: parent;
            delegate: Rectangle {
                width: parent.width
                height: 30
                color: "transparent"
                Text {
                    anchors { verticalCenter: parent.verticalCenter; left: parent.left; leftMargin: 10 }
                    text: modelData
                    font.family: "Helvetica Neue"
                }

                Rectangle {
                    anchors { left: parent.left; bottom: parent.bottom; right: parent.right; leftMargin: 5; rightMargin: 5 }
                    height: 1
                    color: "lightgray"
                }

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        if (list.currentIndex != index) {
                            list.currentIndex = index;
                            root.selected(modelData)
                        }
                        root.state = ""
                    }
                }
            }

            highlight: Rectangle {
                width: parent.width
                radius: 5
                color: "orangered"
                opacity: 0.25
            }
        }
    }

    states: State {
        name: "EXPANDED"
        PropertyChanges { target: listContainer; opacity: 1.0 }
        PropertyChanges { target: root; width: parent.width; height: parent.height; anchors.margins: 0 }
        PropertyChanges { target: backdrop; color: "black"; opacity: 0.5 }
        PropertyChanges { target: button; opacity: 0.0 }
    }
}
