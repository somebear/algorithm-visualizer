/*

    Copyright (c) 2012, Jonas Rabbe
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this
       list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
    ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

import QtQuick 1.0
import VisualizerLibrary 1.0

Rectangle {
    width: 640
    height: 480
    color: "white"

    VisualizerItem {
        id: theItem
        anchors { top: parent.top; left: parent.left; right: parent.right; bottom: buttonRow.top }
        anchors.margins: 10

        Text {
            anchors { left: parent.left; top: parent.top; margins: 10 }
            text: "<b>" + theItem.width + "</b> elements"
            color: "white"
            font.family: "Helvetica Neue"
        }

        Text {
            id: finishedText
            color: "white"
            text: ""
            font.family: "Helvetica Neue"
            horizontalAlignment: Text.AlignHCenter
            anchors { left: parent.left; top: parent.top; leftMargin: parent.width / 2 - width / 2; topMargin: parent.height / 2 - height / 2 }
        }

        onAlgorithmCompleted: finishedText.text = "<b>Finished</b>" + (runningTime > 0 ? "<br>Completed in " + msecToPretty(runningTime) : "")
    }

    function msecToPretty(msec) {
        if (msec > 1000) {
            var sec = Math.round((msec / 1000) * 10) / 10;
            return "" + sec + " sec";
        } else {
            return "" + msec + " msec";
        }
    }

    Row {
        id: buttonRow
        spacing: 5;
        anchors.margins: 10
        anchors { /*top: theItem.bottom; */bottom: parent.bottom; right: parent.right }

        Button {
            text: "Reset"
            onClicked: {
                theItem.reset();
                finishedText.text = "";
            }
        }

        Button {
            text: theItem.active ? "Stop" : "Start"
            onClicked: if (theItem.active) {
                           theItem.stop();
                       } else {
                           theItem.start();
                           finishedText.text = "";
                       }
        }
    }

    SelectionList {
        anchors { left: parent.left; bottom: parent.bottom }
        anchors.margins: 10
        text: theItem.selectedAlgorithm
        model: theItem.algorithms
        onSelected: {
            theItem.selectedAlgorithm = item;
            finishedText.text = "";
        }
    }

}
