/*

    Copyright (c) 2012, Jonas Rabbe
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this
       list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
    ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef SORTINGITEM_H
#define SORTINGITEM_H

#include <qdeclarativeitem.h>

#include "visualizerdata.h"
#include "abstractsortingplugin.h"

#include <qshareddata.h>
#include <qstringlist.h>
#include <qmap.h>

class VisualizerItem : public QDeclarativeItem
{
    Q_OBJECT

    Q_PROPERTY(bool active READ isActive NOTIFY activeChanged)
    Q_PROPERTY(QStringList algorithms READ algorithms NOTIFY algorithmsChanged)
    Q_PROPERTY(QString selectedAlgorithm READ selectedAlgorithm WRITE setSelectedAlgorithm NOTIFY selectedAlgorithmChanged)
    Q_PROPERTY(qreal width READ width WRITE setWidth NOTIFY widthChanged)
    Q_PROPERTY(qreal height READ height WRITE setHeight NOTIFY heightChanged)
    Q_PROPERTY(int runningTime READ runningTime)

public:
    explicit VisualizerItem(QDeclarativeItem *parent = 0);

    void paint(QPainter *, const QStyleOptionGraphicsItem *, QWidget *);

    Q_INVOKABLE void start();
    Q_INVOKABLE void stop();
    Q_INVOKABLE void reset();

    bool isActive() const;
    QStringList algorithms();

    QString selectedAlgorithm() const;
    void setSelectedAlgorithm(QString key);

    int runningTime() const;

signals:
    void activeChanged();
    void algorithmsChanged();
    void selectedAlgorithmChanged();
    void widthChanged();
    void heightChanged();
    void algorithmCompleted();

protected:
    void geometryChanged(const QRectF &newGeometry, const QRectF &oldGeometry);

private slots:
    void doUpdate();
    void handleRunningTime(int msec);

private: // methods
    void loadPlugins();
    bool handlePlugin(QObject *object);

private: // data
    QExplicitlySharedDataPointer<VisualizerData> m_data;
    QMap<QString, AbstractSortingPlugin *> m_algorithms;
    AbstractSortingPlugin *m_selectedAlgorithm;
    int m_runningTime;
};

#endif // SORTINGITEM_H
