/*

    Copyright (c) 2012, Jonas Rabbe
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this
       list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
    ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#include "visualizerdata.h"
#include <QTime>

VisualizerData::VisualizerData()
{
    qsrand(QTime::currentTime().msec());
}

VisualizerData::VisualizerData(const VisualizerData &other)
    : QSharedData(other)
    , m_data(other.data())
{
}

void VisualizerData::resize(int length)
{
    QMutexLocker locker(&m_mutex);

    if (m_data.length() == length)
        return;

    doReset(length);
}

void VisualizerData::reset()
{
    QMutexLocker locker(&m_mutex);

    doReset(m_data.length());
}

int VisualizerData::at(int i) const
{
    QMutexLocker locker(&m_mutex);

    return m_data.at(i);
}

int VisualizerData::length() const
{
    QMutexLocker locker(&m_mutex);

    return m_data.length();
}

void VisualizerData::swap(int i, int j)
{
    QMutexLocker locker(&m_mutex);

    m_data.swap(i, j);
}

void VisualizerData::replace(int i, int value)
{
    QMutexLocker locker(&m_mutex);

    m_data.replace(i, value);
}

const QList<int> &VisualizerData::data() const
{
    QMutexLocker locker(&m_mutex);

    return m_data;
}

// NOTE: Must be called from a method that has locked m_mutex!
void VisualizerData::doReset(int length)
{
    m_data.clear();

    for (int i = 0; i < length; i++) {
        int value = (qrand() % MAX_DATA_VALUE) + 1;
        m_data.append(value);
    }
}
