TEMPLATE = lib
TARGET = SortingPluginLib

HEADERS += \
    abstractsortingplugin.h \
    visualizerdata.h \

SOURCES += \
    abstractsortingplugin.cpp \
    visualizerdata.cpp \

mac {
    CONFIG += lib_bundle
    QMAKE_LFLAGS_SONAME  = -Wl,-install_name,@executable_path/../Frameworks/

    target.path += $$OUT_PWD/../AlgorithmVisualizer/AlgorithmVisualizer.app/Contents/Frameworks
    INSTALLS += target
}
