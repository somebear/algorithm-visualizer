/*

    Copyright (c) 2012, Jonas Rabbe
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this
       list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
    ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#include "stoogesort.h"
#include "visualizerdata.h"
#include <qplugin.h>

StoogeSort::StoogeSort(QObject *parent)
    : AbstractSortingPlugin(parent)
{
}

QString StoogeSort::name() const
{
    return QString("Stooge Sort");
}

bool StoogeSort::sort()
{
    stoogesort(0, m_data->length() - 1);

    if (m_stop)
        return false;

    return true;
}

void StoogeSort::stoogesort(int i, int j)
{
    if (m_stop)
        return;

    if (m_data->at(j) < m_data->at(i)){
        m_data->swap(i, j);
    }

    doUpdate();

    if ((j - i + 1) >= 3) {
        int t = (j - i + 1) / 3;
        stoogesort(i, j - t);
        stoogesort(i + t, j);
        stoogesort(i, j - t);
    }
}

Q_EXPORT_PLUGIN2(stoogesort, StoogeSort)
