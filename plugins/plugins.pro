TEMPLATE=subdirs

SUBDIRS = \
    insertionsort \
    quicksort \
    bubblesort \
    selectionsort \
    heapsort \
    bogosort \
    cocktailsort \
    stoogesort \
    quicksort_lomuto \
    shellsort \
    combsort \
    timsort \
    mergesort \

