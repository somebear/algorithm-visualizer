/*

    Copyright (c) 2012, Jonas Rabbe
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this
       list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
    ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#include "combsort.h"
#include "visualizerdata.h"
#include <qplugin.h>

CombSort::CombSort(QObject *parent)
    : AbstractSortingPlugin(parent)
{
}

QString CombSort::name() const
{
    return QString("Comb Sort");
}

bool CombSort::sort()
{
    int gap = m_data->length();
    bool swapped = false;

    while ((gap > 1) || swapped) {
        gap = (int)((qreal)gap / 1.247330950103979);
        if (gap < 1)
            gap = 1;

        int i = 0;
        swapped = false;
        while (i + gap < m_data->length()) {
            if (m_data->at(i) > m_data->at(i + gap)) {
                m_data->swap(i, i + gap);
                swapped = true;
            }

            if (m_stop)
                return false;

            doUpdate();
            i++;
        }
        doUpdate();
    }

    return true;
}

Q_EXPORT_PLUGIN2(combsort, CombSort)
