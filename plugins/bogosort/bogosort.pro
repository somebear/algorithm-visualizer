TEMPLATE = lib

CONFIG += plugin

INCLUDEPATH += ../../SortingPluginLib

HEADERS += \
    bogosort.h

SOURCES += \
    bogosort.cpp

mac {
    LIBS += -F../../SortingPluginLib -framework SortingPluginLib

    target.path += $$OUT_PWD/../../AlgorithmVisualizer/AlgorithmVisualizer.app/Contents/PlugIns
    INSTALLS += target
} else {
    LIBS += -L../../SortingPluginLib -lSortingPluginLib
}

