TEMPLATE = lib

CONFIG += plugin

INCLUDEPATH += ../../SortingPluginLib

HEADERS += \
    selectionsort.h

SOURCES += \
    selectionsort.cpp

mac {
    LIBS += -F../../SortingPluginLib -framework SortingPluginLib

    target.path += $$OUT_PWD/../../AlgorithmVisualizer/AlgorithmVisualizer.app/Contents/PlugIns
    INSTALLS += target
} else {
    LIBS += -L../../SortingPluginLib -lSortingPluginLib
}

