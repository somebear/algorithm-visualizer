/*

    Copyright (c) 2012, Jonas Rabbe
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this
       list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
    ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#include "bubblesort.h"
#include "visualizerdata.h"
#include <qplugin.h>

BubbleSort::BubbleSort(QObject *parent)
    : AbstractSortingPlugin(parent)
{
}

QString BubbleSort::name() const
{
    return QString("Bubble Sort");
}

bool BubbleSort::sort()
{
    int n = m_data->length();
    do {
        int newn = 0;
        for (int i = 1; i < n; i++) {
            if (m_data->at(i-1) > m_data->at(i)) {
                m_data->swap(i-1, i);
                newn = i;
            }

            if (m_stop)
                return false;

            doUpdate();
        }
        n = newn;

        doUpdate();
    } while (n > 0);

    return true;
}

Q_EXPORT_PLUGIN2(bubblesort, BubbleSort)
