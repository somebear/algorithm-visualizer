/*

    Copyright (c) 2012, Jonas Rabbe
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this
       list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
    ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#include "quicksort_lomuto.h"
#include "visualizerdata.h"
#include <qplugin.h>

QuickSort_Lomuto::QuickSort_Lomuto(QObject *parent)
    : AbstractSortingPlugin(parent)
{
}

QString QuickSort_Lomuto::name() const
{
    return QString("Quick Sort (Lomuto)");
}

bool QuickSort_Lomuto::sort()
{
    quicksort(0, m_data->length() - 1);

    if (m_stop)
        return false;

    return true;
}

void QuickSort_Lomuto::quicksort(int p, int r)
{
    doUpdate();

    if (p < r) {
        int q = partition(p, r);

        if (m_stop)
            return;

        doUpdate();

        quicksort(p, q);
        quicksort(q + 1, r);
    }
}

int QuickSort_Lomuto::partition(int p, int r)
{
    int x = m_data->at(r);
    int i = p - 1;

    for (int j = p; j <= r; j++) {
        if (m_data->at(j) <= x) {
            i++;
            m_data->swap(i, j);
        }

        doUpdate();
    }

    if (i < r)
        return i;
    else
        return i - 1;
}

Q_EXPORT_PLUGIN2(quicksortlomuto, QuickSort_Lomuto)
