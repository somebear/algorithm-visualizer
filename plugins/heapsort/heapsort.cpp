/*

    Copyright (c) 2012, Jonas Rabbe
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this
       list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
    ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#include "heapsort.h"
#include "visualizerdata.h"
#include <qplugin.h>

HeapSort::HeapSort(QObject *parent)
    : AbstractSortingPlugin(parent)
{
}

QString HeapSort::name() const
{
    return QString("Heap Sort");
}

bool HeapSort::sort()
{
    heapify();

    int end = m_data->length() - 1;
    while (end > 0) {
        m_data->swap(end, 0);
        end--;
        siftDown(0, end);

        if (m_stop)
            return false;

        doUpdate();
    }

    return true;
}

void HeapSort::heapify()
{
    int start = (m_data->length() - 1) / 2;

    while (start >= 0) {
        siftDown(start, m_data->length() - 1);
        start--;

        doUpdate();
    }
}

void HeapSort::siftDown(int start, int end)
{
    int root = start;

    while ((root * 2 + 1) <= end) {
        int child = root * 2 + 1;
        int swap = root;

        if (m_data->at(swap) < m_data->at(child))
            swap = child;

        if ((child + 1 <= end) && (m_data->at(swap) < m_data->at(child + 1)))
            swap = child + 1;

        if (swap != root) {
            m_data->swap(root, swap);
            root = swap;
        } else {
            return;
        }

        doUpdate();
    }
}

Q_EXPORT_PLUGIN2(heapsort, HeapSort)
