/*

    Copyright (c) 2012, Jonas Rabbe
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this
       list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
    ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#include "quicksort.h"
#include "visualizerdata.h"
#include <qplugin.h>

QuickSort::QuickSort(QObject *parent)
    : AbstractSortingPlugin(parent)
{
}

QString QuickSort::name() const
{
    return QString("Quick Sort");
}

bool QuickSort::sort()
{
    quicksort(0, m_data->length() - 1);

    if (m_stop)
        return false;

    return true;
}

void QuickSort::quicksort(int p, int r)
{
    doUpdate();

    if (p < r) {
        int q = partition(p, r);

        if (m_stop)
            return;

        doUpdate();

        quicksort(p, q);
        quicksort(q + 1, r);
    }
}

int QuickSort::partition(int p, int r)
{
    int x = m_data->at(p);
    int i = p - 1;
    int j = r + 1;

    while (true) {
        do {
            j--;
            doUpdate();
        } while (m_data->at(j) > x);
        do {
            i++;
            doUpdate();
        } while (m_data->at(i) < x);

        if (i < j) {
            m_data->swap(i, j);
        } else {
            return j;
        }

        doUpdate();

        if (m_stop)
            return 0;
    }
}

Q_EXPORT_PLUGIN2(quicksort, QuickSort)
