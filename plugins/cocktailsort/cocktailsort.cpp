/*

    Copyright (c) 2012, Jonas Rabbe
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this
       list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
    ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#include "cocktailsort.h"
#include "visualizerdata.h"
#include <qplugin.h>

CocktailSort::CocktailSort(QObject *parent)
    : AbstractSortingPlugin(parent)
{
}

QString CocktailSort::name() const
{
    return QString("Cocktail Sort");
}

bool CocktailSort::sort()
{
    int begin = 0;
    int end = m_data->length();
    do {
        int newend = 0;
        for (int i = 1; i < end; i++) {
            if (m_data->at(i-1) > m_data->at(i)) {
                m_data->swap(i-1, i);
                newend = i;
            }

            if (m_stop)
                return false;

            doUpdate();
        }
        end = newend;
        int newbegin = end;
        for (int i = end - 1; i >= begin; i--) {
            if (m_data->at(i+1) < m_data->at(i)) {
                m_data->swap(i+1, i);
                newbegin = i;
            }

            if (m_stop)
                return false;

            doUpdate();
        }
        begin = newbegin;

        doUpdate();
    } while (begin < end);

    return true;
}

Q_EXPORT_PLUGIN2(cocktailsort, CocktailSort)
