TEMPLATE = lib

CONFIG += plugin

INCLUDEPATH += ../../SortingPluginLib

HEADERS += \
    cocktailsort.h

SOURCES += \
    cocktailsort.cpp

mac {
    LIBS += -F../../SortingPluginLib -framework SortingPluginLib

    target.path += $$OUT_PWD/../../AlgorithmVisualizer/AlgorithmVisualizer.app/Contents/PlugIns
    INSTALLS += target
} else {
    LIBS += -L../../SortingPluginLib -lSortingPluginLib
}

