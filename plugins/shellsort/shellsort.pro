TEMPLATE = lib

CONFIG += plugin

INCLUDEPATH += ../../SortingPluginLib

HEADERS += \
    shellsort.h

SOURCES += \
    shellsort.cpp

mac {
    LIBS += -F../../SortingPluginLib -framework SortingPluginLib

    target.path += $$OUT_PWD/../../AlgorithmVisualizer/AlgorithmVisualizer.app/Contents/PlugIns
    INSTALLS += target
} else {
    LIBS += -L../../SortingPluginLib -lSortingPluginLib
}

